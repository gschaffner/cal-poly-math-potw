\documentclass{article}
\usepackage[]{geometry} % page settings
\usepackage{amsmath, amssymb, physics} % provides many mathematical environments & tools

\setlength{\parindent}{0mm}

\begin{document}
\title{Proposed Cal Poly Mathematics Puzzle of the Week 2}
\author{Ganden Schaffner}
\date{February 8, 2019}
\maketitle

\section*{Problem}

Find the smallest positive $A \in \mathbb{R}$ in terms of $k$, given that

$$A = 2 \int_0^A \frac{1}{1+\qty(\tan(x))^k} \dd{x}$$

with $k$ a nonzero real number.

\section*{Solution}
\subsection*{Proof of the king property of integration}
First, we prove an interesting property of definite integrals (the king property of integration). Namely, that:

$$\int_a^b f(x) \dd{x} = \int_a^b f(a+b-x) \dd{x}$$

Consider $\int_a^b f(a+b-x) \dd{x}$. We perform u-substitution:

$$u = a+b-x$$
$$du = -dx$$

Then:

$$\int_a^b f(a+b-x) \dd{x} = \int_b^a -f(u) \dd{u} = \int_a^b f(u) \dd{u} = \int_a^b f(x) \dd{x}$$

\newpage

\subsection*{A solution $A$}
Now we may apply this to the problem. We first consider the case where $k$ is an even integer:

\begin{align*}
    A &= 2 \int_0^A \frac{1}{1+\qty(\tan(x))^k} \dd{x}\\
    A &= 2 \int_0^A \frac{1}{1+\qty(\tan(A-x))^k} \dd{x}
\end{align*}

We notice that the above integrand must have even symmetry about A. (This is because of the nature of the odd symmetry of $\tan(x)$ about $x=0$, and because we are working under the case where $k$ is even.) Then:

\begin{align*}
    A &= 2 \int_0^A \frac{1}{1+\qty(\tan(A-x))^k} \dd{x}\\
    A &= \int_0^{2A} \frac{1}{1+\qty(\tan(A-x))^k} \dd{x} \tag*{by symmetry about A}\\
    A &= \int_0^{2A} \frac{1}{1+\qty(\tan(A-\qty(2A-x)))^k} \tag*{by the king property}\\
    A &= \int_0^{2A} \frac{1}{1+\qty(\tan(x-A))^k} \dd{x}
\end{align*}

Again, we notice that the above integrand must have even symmetry about A. Then:

\begin{align*}
    A &= \int_0^{2A} \frac{1}{1+\qty(\tan(x-A))^k} \dd{x}\\
    A &= 2 \int_A^{2A} \frac{1}{1+\qty(\tan(x-A))^k} \dd{x} \tag*{by symmetry about A}\\
    A &= 2 \int_A^{2A} \frac{1}{1+\qty(\tan(x))^k} \dd{x} \tag*{by the king property}
\end{align*}

Now, we have shown that:

$$A = 2 \int_0^A \frac{1}{1+\qty(\tan(x))^k} \dd{x} = 2 \int_A^{2A} \frac{1}{1+\qty(\tan(x))^k} \dd{x}$$

Since $k$ is even, the above line implies that $\tan(x)$ will have either even or odd symmetry about $A$ on the interval $x \in [0,2A]$. Thus, $A$ must be one of $A = \frac{\pi}{2}n, n \in \mathbb{Z}$. We are looking for the smallest positive $A$ so try $A = \frac{\pi}{2}$.

\newpage

Let $I$ be the integral of the problem statement. We will later check whether $\frac{\pi}{2} = 2I$.

\begin{align*}
    I &= \int_0^{\frac{\pi}{2}} \frac{1}{1+\qty(\tan(x))^k} \dd{x}\\
    I &= \int_0^{\frac{\pi}{2}} \frac{1}{1+\qty(\tan(\frac{\pi}{2}-x))^k} \dd{x} \tag*{by the king property}\\
    I &= \int_0^{\frac{\pi}{2}} \frac{1}{1+\qty(\cot(x))^k} \dd{x}\\
    I &= \int_0^{\frac{\pi}{2}} \frac{1}{1+\frac{1}{\qty(\tan(x))^k}} \dd{x}\\
    I &= \int_0^{\frac{\pi}{2}} \frac{\qty(\tan(x))^k}{\qty(\tan(x))^k+1} \dd{x}
\end{align*}

Now we note that two of the above integrands have the same denominator. We write $2I$ as:

\begin{align*}
    2I &= \int_0^{\frac{\pi}{2}} \frac{1}{1+\qty(\tan(x))^k} \dd{x} + \int_0^{\frac{\pi}{2}} \frac{\qty(\tan(x))^k}{\qty(\tan(x))^k+1} \dd{x}\\
    2I &= \int_0^{\frac{\pi}{2}} \frac{1+\qty(\tan(x))^k}{1+\qty(\tan(x))^k} \dd{x}\\
    2I &= \int_0^{\frac{\pi}{2}} \dd{x}\\
    2I &= \frac{\pi}{2}
\end{align*}

This confirms that $A = \frac{\pi}{2}$ is a solution.

\newpage

\subsection*{Showing that $A=\frac{\pi}{2}$ is the smallest positive solution}
Now we aim to prove that this is the smallest positive solution. \newline

In order to do this, we will note the definition of an average value of a function $f(x)$ from $x=a$ to $x=b$.

$$f_\text{av}(a,b) = \frac{1}{b-a} \int_a^b f(x) \dd{x}$$

Now, allow $f(x) = \frac{1}{1+\qty(\tan(x))^k}$. Then, note that the average value from $x=0$ to $x=A$ is:

$$f_\text{av}(0,A) = \frac{1}{A} \int_0^A f(x) \dd{x} = \frac{1}{A} \int_0^A \frac{1}{1+\qty(\tan(x))^k} \dd{x}$$

The problem, then, can be rewritten: Find the smallest positive $A \in \mathbb{R}$ in terms of $k$ such that the average of $f(x) = \frac{1}{1+\qty(\tan(x))^k}$ from $x=0$ to $x=A$ is $\frac{1}{2}$. ($k$ is a nonzero real number.)

Now, by the definition of the average value, we know that $f_\text{av}(0,0) = f(0)$. \newline

We also know, as shown in the prior section, that $f_\text{av}(0,A) = \frac{1}{2}$. \newline

Now we will consider two separate cases: $k>0$ and $k<0$. \newline

When $k>0$, $f(\frac{\pi}{2}) = \frac{1}{1+\infty^k} = 1$. So, on the interval $x=0$ to $x=\frac{\pi}{2}$, $f(x)$ goes from 1 to 0 with an average value of $\frac{1}{2}$. If we can show that $f(x)$ is always decreasing on this interval, then it follows that $f(x)$ will always be ``pulling down'' $f_\text{av}(0,x)$ on this interval (since $f(0)$ = $f_\text{av}(0,0)$). This would imply that $f_\text{av}(0,x)$ is always decreasing on this interval, and furthermore, since $f_\text{av}(0,x)$ begins at $1$, that $x=\frac{\pi}{2}$ is the first positive $x$ for which $f_\text{av}(0,x)$ is $\frac{1}{2}$. \newline

It is easy to see that $f(x)$ is monotonically decreasing from $x=0$ to $x=\frac{\pi}{2}$ when $k>0$. $\tan(x)$ is increasing on this interval, so $\qty(\tan(x))^k$ is increasing on this interval (as $k<0$). So $f(x) = \frac{1}{1+\qty(\tan(x))^k}$ is decreasing on this interval. \newline

When $k<0$, we can apply a similar argument. Instead, however, we will see that $f(x)$ ranges from 0 to 1, and $f_\text{av}(0, x)$ ranges from 0 to $\frac{1}{2}$. $\tan(x)$ is increasing on this interval, so $\qty(\tan(x))^k$ is decreasing on this interval. So $f(x) = \frac{1}{1+\qty(\tan(x))^k}$ is increasing (monotonically) on this interval. \newline

Thus, for any nonzero real $k$, $A = \frac{\pi}{2}$ is the smallest positive solution.

\newpage

\section*{An alternate method of finding a solution $A$}
This method was found by Patrick Tran, a colleague/fellow student of mine. \newline

Note that this method still requires the third part of the above solution. It does not alone show that $A=\frac{\pi}{2}$ is the smallest positive solution.

\begin{align*}
    A &= 2 \int_0^A \frac{1}{1+\qty(\tan(x))^k} \dd{x}\\
    A &= 2 \int_0^A \frac{\qty(1+\qty(\tan(x))^k)-\qty(\tan(x))^k}{1+\qty(\tan(x))^k} \dd{x}\\
    A &= 2 \int_0^A \qty(1 - \frac{\qty(\tan(x))^k}{1+\qty(\tan(x))^k}) \dd{x}\\
    A &= 2A - 2 \int_0^A \frac{\qty(\tan(x))^k}{1+\qty(\tan(x))^k} \dd{x}\\
    A &= 2 \int_0^A \frac{\qty(\tan(x))^k}{1+\qty(\tan(x))^k} \dd{x}
\end{align*}

Now, subtract this from the original equation:

$$0 = \int_0^A \frac{1-\qty(\tan(x))^k}{1+\qty(\tan(x))^k} \dd{x}$$

Graphing the above integrand suggests that it is odd about $\frac{\pi}{4}$ on the interval $[0,\frac{\pi}{2}]$. If we can prove this to be the case, then $A=\frac{\pi}{2}$ would be a solution. However, tedious algebra is required to show that this function is odd. Instead, we simply check whether $A=\frac{\pi}{2}$ is a solution of the original equation.

$$\vdots$$

Thus, for any nonzero real $k$, $A = \frac{\pi}{2}$ is the smallest positive solution.

\section*{Footnote}
This problem was inspired by an old Putnam Exam problem which asked students to find the integral:

$$\int_0^{\frac{\pi}{2}} \frac{1}{1+\qty(\tan(x))^{\sqrt{2}}} \dd{x}$$

\end{document}
