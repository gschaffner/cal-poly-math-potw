\documentclass{article}
\usepackage[]{geometry} % page settings
\usepackage{amsmath, amssymb} % provides many mathematical environments & tools

\setlength{\parindent}{0mm}

\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

\begin{document}
\title{Cal Poly Mathematics Puzzle of the Week}
\author{G. Schaffner}
\date{April 24 - May 3, 2018}
\maketitle

\subsection*{Problem}
For $n = 1, 2, 3...$ let $c_n = n(n+1)/6$ and let $f_n$ denote the polynomial function defined by

$$f_n(x) = -c_n + \sum_{i=1}^{n} i x^i .$$

The tangent line to the graph $y=f_n(x)$ at $x=1$ intersects the $x$-axis at the point $x_n$. Find

$$\lim_{n\to\infty} n \prod_{i=1}^{n} x_i  .$$

\subsection*{Solution}
We begin by finding the tangent line to $f_n(x)$ at $x=1$.
\begin{align*}
	L_n(x) &= (x-1) \left[\frac{df_n}{dx}\right]_{x=1} + f_n(1)\\
		&= (x-1) \left[\sum_{i=1}^{n} i^2 x^{i-1}\right]_{x=1} + f_n(1)\\
		&= (x-1) \sum_{i=1}^{n} i^2 - \frac{n(n+1)}{6} + \left[\sum_{i=1}^{n} i x^i\right]_{x=1}\\
		&= (x-1) \sum_{i=1}^{n} i^2 - \frac{n(n+1)}{6} + \sum_{i=1}^{n} i\\
		&= (x-1) \sum_{i=1}^{n} i^2 - \frac{n(n+1)}{6} + \frac{n(n+1)}{2} \numberthis \label{eq:L(x) with i^2 sum}
\end{align*}
Let us find a polynomial to represent $\sum_{i=1}^{n} i^2$.
\begin{align*}
	\sum_{i=1}^{n}{i^2} &= \sum_{i=0}^{n} i^2\\
		&= \int_0^n x^2 dx + E_n \tag*{Where $E_n$ is the error}\\
		&= \frac{n^3}{3} + E_n \numberthis \label{eq:i^2 sum with E_n}
\end{align*}
The error is the difference between the area of each Riemann rectangle (with $\Delta x = 1$) and the area under the curve $y=x^2$.
\begin{align*}
	E_n &= \sum_{i=1}^{n} \left(i^2 - \int_{i-1}^{i}{x^2}{dx}\right)\\
		&= \sum_{i=1}^{n} \left(i^2 - \left[\frac{x^3}{3}\right]_{i-1}^i\right)\\
		&= \sum_{i=1}^{n} \left(i^2 - \left(\frac{i^3}{3} - \frac{i^3-3i^2+3i-1}{3}\right)\right)\\
		&= \sum_{i=1}^{n} \left(i - \frac{1}{3}\right)\\
		&= \frac{n(n+1)}{2} - \frac{n}{3}\\
		&= \frac{n^2}{2} + \frac{n}{6} \numberthis \label{eq:E_n}
\end{align*}
We combine equations \ref{eq:i^2 sum with E_n} and \ref{eq:E_n} to yield:
\begin{align*}
	\sum_{i=1}^{n} i^2 &= \frac{n^3}{3} + \frac{n^2}{2} + \frac{n}{6}\\
		&= \frac{n(n+1)(2n+1)}{6} \numberthis \label{eq:i^2 sum}
\end{align*}
We combine equations \ref{eq:L(x) with i^2 sum} and \ref{eq:i^2 sum} to yield:
\begin{align*}
	L_n(x) &= (x-1) \frac{n(n+1)(2n+1)}{6} - \frac{n(n+1)}{6} + \frac{n(n+1)}{2} \numberthis \label{eq:L_n(x)}
\end{align*}
Knowing that $L_n(x_n) = 0$, we can solve equation \ref{eq:L_n(x)} for $x_n$:
\begin{align*}
	x_n &= \frac{2n-1}{2n+1}
\end{align*}
We can now rewrite the limit:
\begin{align*}
	\lim_{n\to\infty} n \prod_{i=1}^{n}{x_i} &= \lim_{n\to\infty} n \prod_{i=1}^{n} \frac{2i-1}{2i+1}
\end{align*}
Investigating the product $\prod_{i=1}^{n} \frac{2i-1}{2i+1}$, we notice the trend:
\begin{align*}
	\prod_{i=1}^{n} \frac{2i-1}{2i+1} &= \frac{1}{3} \cdot \frac{3}{5} \cdot \frac{5}{7} \cdots \frac{2(n-2)-1}{2(n-2)+1} \cdot \frac{2(n-1)-1}{2(n-1)+1} \cdot \frac{2n-1}{2n+1}\\
		&= \frac{1}{3} \cdot \frac{3}{5} \cdot \frac{5}{7} \cdots \frac{2n-5}{2n-3} \cdot \frac{2n-3}{2n-1} \cdot \frac{2n-1}{2n+1}\\
		&= \frac{1}{2n+1}
\end{align*}
We can now rewrite the limit:
\begin{align*}
	\lim_{n\to\infty} n \prod_{i=1}^{n} x_i &= \lim_{n\to\infty} \frac{n}{2n+1}
\end{align*}
We write $g(x) = \frac{x}{2x+1}$, a function of x continuous for $x \geq 1 \ni g(n) = \frac{n}{2n+1} \forall n \in \mathbb{N}^+$. Thus:
\begin{align*}
	\lim_{n\to\infty} n \prod_{i=1}^{n} x_i = \lim_{n\to\infty} \frac{n}{2n+1} &= \lim_{x\to\infty} g(x)\\
		&= \lim_{x\to\infty} \frac{x}{2x+1}\\
		&= \frac{1}{2} \tag*{By L'H\^{o}pital's rule}
\end{align*}
\end{document}
