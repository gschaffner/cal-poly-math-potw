\documentclass{article}
\usepackage[]{geometry} % page settings
\usepackage{amsmath, amssymb, mathtools} % for many mathematical environments & tools
\usepackage[arrowdel]{physics} % for more mathematical tools
\usepackage{esint} % for more mathematical tools

\setlength{\parindent}{0mm}

\begin{document}
\title{Cal Poly Mathematics Puzzle of the Week}
\author{Ganden Schaffner}
\date{January 31 - February 12, 2020}
\maketitle

\section*{Problem}
Let $c$ denote the graph of a cubic polynomial, and let $P$ be a point on $c$. Let $\ell_P$ be the tangent line at $P$, and let $Q$ be the point where $\ell_P$ meets $c$ again. Finally let $\ell_Q$ denote the tangent line at $Q$. If $A$ is the area between $\ell_P$ and $c$, and $B$ denotes the area between $\ell_Q$ and $c$, what is the relationship between $A$ and $B$?

\section*{Solution}
Since a tangent line to $c$ at $x=p$ is a first order approximation of $c$, the error in the approximation must be at least second order in $x-p$. That is,

\[
    c-\ell_P = \alpha(x-p)^2(x-q)
\]

since $q$ is the other intersection between $q$ and $p$. Similarly, for the tangent line to $c$ at $x=q$ we have

\[
    c-\ell_Q = \beta (x-q)^2(x-r)
\]

where $r$ is the other intersection between $c$ and $\ell_Q$. (If further rigor is desired, it can be easily proven that for any real polynomial $f$, $f(a)=f'(a)=0 \iff f(x)$ has (at least) a double root at $x=a$. I won't prove this, as I don't formally rely on it here.)

I will ignore the case where $c-\ell_P$ has a triple root at $x=p$ (i.e. $P$ is the inflection point of $c$), as this case results in the areas $A$ and $B$ being identical and unbounded on one side.

Without loss of generality, take

\[
    c(x) = b_0+b_1x+b_2x^2+b_3x^3.
\]

Then $\ell_P$ and $\ell_Q$ are given by

\[
    \ell_P(x) = c(p)+(x-p)c'(p) \qq{and} \ell_Q(x) = c(q)+(x-q)c'(q)
\]

and so we can write the error $c-\ell_P$ and factor it as

\begin{gather*}
    c-\ell_P = b_0+b_1x+b_2x^2+b_3x^3-((x-p)(b_1+2b_2p+3b_3p^2)+b_0+b_1p+b_2p^2+b_3p^3)\\
    c-\ell_P = b_3(x-p)^2\qty(x+\frac{b_2}{b_3}+2p).
\end{gather*}

Similarly for $c-\ell_Q$ we have

\[
    c-\ell_Q = b_3(x-q)^2\qty(x+\frac{b_2}{b_3}+2q).
\]

since $x=q$ is a root of $\ell_P$, we have

\[
    q = -\frac{b_2}{b_3}-2p.
\]

Since $x=r$ is a root of $\ell_Q$, we have

\[
    r = -\frac{b_2}{b_3}-2q = \frac{b_2}{b_3}+4p.
\]

Interestingly, note that $p=(q+r)/2$ for any cubic and choice of $p$.

The areas are then given by

\begin{gather*}
    A = \abs{\int_{p}^{q}\dd{x} (c-\ell_P)} \qq{and} B = \abs{\int_{q}^{r}\dd{x} (c-\ell_Q)}\\
    A = \abs{\int_{p}^{q}\dd{x} b_3(x-p)^2\qty(x+\frac{b_2}{b_3}+2p)} \qq{and} B = \abs{\int_{q}^{r}\dd{x} b_3(x-q)^2\qty(x+\frac{b_2}{b_3}+2q)}
\end{gather*}

Note that the absolute values may be outside of the integral, as $c-\ell_P$ does not change in sign between its roots $p$ and $q$. The same argument applies for area $B$.

Also note the symmetry between the two integrals --- they are identical with the change $q \to r$ and $p \to q$. We will therefore just consider $A$. Integrating, we have

\[
    \int_{p}^{q}\dd{x} b_3(x-p)^2(x+\frac{b_2}{b_3}+2p) = -\frac{b_3}{12}(q-p)^4,
\]

and so we have

\[
    A = \frac{b_3}{12}(q-p)^4 \qq{and} B = \frac{b_3}{12}(r-q)^4.
\]

Since $q$, $p$, and $r$ are evenly spaced with $p$ between $q$ and $r$, we have $q-p=2(q-r)$. So

\[
    \boxed{B = 16A}.
\]
\end{document}
